<?php
namespace SISMAREPTB1\View;

if(!isset($_SESSION))
{
    session_start();
}

if (!$_SESSION['login'] == true){
  echo "
    <script>
      window.location.href = 'http://localhost/SISMAREPTB1/'
    </script>
  ";
};

  class ViewPacientes{
    public function __construct(){
    }

    public function listardados($dados_paciente)
    {
      $scripts =  array(
        "mascaras.js",
        "paciente.js"
    );
    $styles =  array(
       "estilo.css"
    );
    $titulo = "Dados do Paciente";
    $pagina = "2";
    $super_usuario = $_SESSION['super_usuario'];
    $administrador = $_SESSION['nome'];


    if(!empty($dados_paciente))
    {
     //Calculando Idade
      if ($dados_paciente["paciente_data_nascimento"]!='0000-00-00') {
       $data       = explode("-",$dados_paciente["paciente_data_nascimento"]);
       $anoNasc    = $data[0];
       $mesNasc    = $data[1];
       $diaNasc    = $data[2];

       $anoAtual   = date("Y");
       $mesAtual   = date("m");
       $diaAtual   = date("d");

       $idade      = $anoAtual - $anoNasc;

       if ($mesAtual < $mesNasc){
        $idade -= 1;
      } elseif ( ($mesAtual == $mesNasc) && ($diaAtual <= $diaNasc) ){
        $idade -= 1;
      }

      if ($idade<=-1) {
        $idade = 0;
      }
    }
    else
    {
      $idade = null;
    }

  

    $conteudo1 = '
    <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Informações</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/SISMAREPTB1/Inicio/agenda">Início</a></li>
            <li class="breadcrumb-item active">Informações</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

  <div class="card card-info">
    <div class="card-header">
      <h3 class="card-title">Informações de Paciente</h3>
    </div>
    <div class="card-body">

     


      <form action=""  method="POST" name="formulario_editar_paciente" id="formulario_editar_paciente" data-toggle="validator"> 
        <div class="form-row">
          <div class="   col-md-2 ">
            <label > Nº de Cadastro:</label>
            <div class="iconInput">
              <i class="fa fa-hashtag"></i>
              <input type="text" name="paciente_numero_cadastro" id="paciente_numero_cadastro" class="form-control edit" value="'.$dados_paciente["paciente_id"].'" Readonly>
              <span class="help-block"></span>
            </div>   
          </div>
        </div>
        <div class="form-row">
          <div class="   col-md-8 ">
            <label > Nome do Paciente:</label>
            <div class="iconInput">
              <i class="fa fa-user"></i>
              <input type="text" name="paciente_nome" id="paciente_nome" class="form-control edit" value="'.$dados_paciente["paciente_nome"].'" required> 
              <span class="help-block"></span>
            </div>   
          </div>
        </div>
        <br>
        <div class="form-row">
          <div class="   col-md-8">
            <label > Cpf:</label>
            <div class="iconInput">
              <i class="fa fa-address-card"></i>
              <input type="text" name="paciente_cpf" id="paciente_cpf" class="form-control edit"  value="'.$dados_paciente["paciente_cpf"].'" required> 
            </div>   
          </div>
        </div>
        <br>
        <div class="form-row">
          <div class="form-group col-md-4 ">
            <label for="inputCity">Telefone/Celular :</label>
            <div class="iconInput"> 
              <i class="fa fa-phone"></i>
              <input type="text" name="paciente_telefone" id="paciente_telefone" class="form-control" placeholder="Digite o número de telefone" value="'.$dados_paciente["paciente_telefone"].'" required>
            </div>
          </div>
        </div>

      <div class="form-row">
        <div class="form-group col-md-3 ">
          <label for="inputCity">Data de Nascimento:</label>
          <div class="iconInput"> 
            <i class="fa fa-calendar-week"></i>
            <input type="date" name="paciente_data_nascimento" id="paciente_data_nascimento" class="form-control edit" value="'.$dados_paciente["paciente_data_nascimento"].'" required>
          </div>
        </div>
        <div class="form-group col-md-2">
          <label for="inputCity">Idade:</label>
          <div class="iconInput"> 
            <i class="fa fa-sort-numeric-up"></i>
            <input type="text" name="paciente_idade" id="paciente_idade" class="form-control edit" value="'.$idade.'"  maxlength="5" disabled>
          </div>
        </div>
      </div> 
      <div class="form-row">
          <div class="col-md-3">
            <label > Data de Cadastro:</label>
            <div class="iconInput">
              <i class="fa fa-calendar-week"></i>
              <input type="date" name="paciente_data_admissao" id="paciente_data_admissao" class="form-control " value="'.$dados_paciente["paciente_data_admissao"].'" required> 
            </div>   
          </div>
        </div><br>
      <div class="form-row">
        <div class="form-group col-md-6 ">
          <label for="inputCity">Rua:</label>
          <div class="iconInput"> 
            <i class="fa fa-map-marker-alt"></i>
            <input type="text" name="paciente_rua" id="paciente_rua" class="form-control edit"  value="'.$dados_paciente["paciente_rua"].'" required>
          </div>
        </div>
        <div class="form-group col-md-2">
          <label for="inputCity">Número:</label>
          <div class="iconInput"> 
            <i class="fa fa-sort-numeric-up"></i>
            <input type="text" name="paciente_numero" id="paciente_numero" class="form-control edit" value="'.$dados_paciente["paciente_numero"].'" maxlength="5" required>
          </div>
        </div>
      </div>

      <div class="form-row">
        <div class="col-md-4 ">
          <label> Bairro:</label> 
          <div class="iconInput"> 
            <i class="fa fa-map-marker-alt"></i>
            <input type="text" name="paciente_bairro" id="paciente_bairro" class="form-control edit"  value="'.$dados_paciente["paciente_bairro"].'" required>
          </div>
        </div>
      </div>
      <br>
      <div class="form-row">
        <div class="form-group col-md-4 ">
          <label for="inputCity">Cidade:</label>
          <div class="iconInput"> 
            <i class="fa fa-map-marker-alt"></i>
            <input type="text" name="paciente_cidade" id="paciente_cidade" class="form-control edit"  value="'.$dados_paciente["paciente_cidade"].'" required>
          </div>
        </div>
        <div class="form-group col-md-3">
          <label for="inputCity">Estado:</label>
          <select id="paciente_estado" name="paciente_estado" class="form-control edit" required>
            <option value =  '.$dados_paciente["paciente_estado"].'>'.$dados_paciente["paciente_estado"].'</option>
          </select>
        </div>
      </div>
     
      <br>
      <div class="form-row">
        <div class=" col-md-6">
          <label > Local de Tratamento:</label>
          <div class="iconInput">
            <i class="fa fa-map-marker-alt"></i>
            <input type="text" name="paciente_local_tratamento" id="paciente_local_tratamento" class="form-control edit"  value="'.$dados_paciente["paciente_local_tratamento"].'" required> 
            <span class="help-block"></span>
          </div>   
        </div>
      </div>
      <br>
      <div class="form-row">
        <div class="col-md-6">
          <label> Observação:</label> 
          <div class="iconInput"> 
            <textarea name="paciente_observacao" id="paciente_observacao" class="col-md-12 edit" style="text-align: left;">
             '.$dados_paciente["paciente_observacao"].'
           </textarea>

         </div>
       </div>
     </div> 
     <br>  
     <div class="form-row ">
      <div>
        <button type="submit" id="btn_editar" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
        <span class="help-block"></span>
      </div>
    </div>


  </form>



</div>
<!-- /.card-body -->
</div>


<!-- /.content -->
</div>
    
    ';
    include 'Templates/template.php';
    
    }
  }

  }
