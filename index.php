<?php
namespace SISMAREPTB1;

require_once './vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface;
use \Psr\Http\Message\ResponseInterface;
use Slim\App;
use SISMAREPTB1\Controller\ControllerAdministrador;

session_start();

$config = ['settings' => [
    'addContentLengthHeader' => false,
]];

$app = new App();

  $app->get('/', function (ServerRequestInterface $request, ResponseInterface $response, $args){
    echo "
    <!DOCTYPE html>
    <html>
    <head>
      <meta charset='utf-8'>
      <meta name='viewport' content='width=device-width, initial-scale=1'>
      <meta http-equiv='x-ua-compatible' content='ie=edge'>
      <link rel='stylesheet' type='text/css' href='public/css/login.css'>
      <title>Login</title>
      <link rel='shortcut icon' href='public/img/logo2.svg' type='image/x-icon' />
    </head>
    <body>
      <div class='container'>
        <div class='box'>
          <img src='public/img/logo_sismarep.png' id='logo'>
          <form class='form-box' method='POST' action='/SISMAREPTB1/Administrador/login' id='form_login'>
            <div class='iconInput'>
              <img src='public/img/user.svg'>
              <input type='email' name='usuario_email' placeholder='Digite seu email' required>	
            </div>
            <div class='iconInput'>
              <img src='public/img/senha.svg'>
              <input type='password' name='usuario_senha' placeholder='Senha' required minlength='6'>
            </div>
            <button class='btn'>Entrar</button>
            
          </form>
          
          <div class='carregando'>
            <h3  class='message2'></h3>
            <h3  class='message'></h3>
          </div>
        </div>
      </div>
      <footer style='position: absolute;bottom: 0;right: 0;left: 0;display: flex;justify-content: center;flex-direction: column;align-items: center;z-index: 1;'>
        <strong style='text-align: center;'>Desenvolvido por <a style='text-decoration: none;' href='#' target='_blanck'>Jefferson | Márcio | Pedro</a></strong>
      <div><strong style='text-align: center;'>Copyright &copy; 2020 <a style='text-decoration: none;' href='#' target='_blanck'>SISMAREP</a>.</strong>
      Todos os direitos reservados</div>
      </footer>
    </body>
    </html>
    ";
  });

  $app->post('/Administrador/{acao}', function (ServerRequestInterface $request, ResponseInterface $response, $args){
    $ControllerAdministrador = new ControllerAdministrador();
    $modulo = 'SISMAREPTB1\\Controller\\ControllerAdministrador';
    $acao = $args['acao'];
    $objeto = new $modulo(null);
    $objeto->$acao();
    if ($ControllerAdministrador->verificaLogin()){
      echo "<script>
        window.location.href = '/SISMAREPTB1/Inicio/agenda';
        </script>
      ";
    }
    else{
      echo "<script>
      alert('Usuário ou senha incorretos!');
        window.location.href = 'http://localhost/SISMAREPTB1/';
        </script>
      ";
    }
  });

  $app->any('/{modulo}/{acao}[/{id}]', function (ServerRequestInterface $request, ResponseInterface $response, $args){
    $ControllerAdministrador = new ControllerAdministrador();
    if ($ControllerAdministrador->verificaLogin()){
      $modulo = 'SISMAREPTB1\\Controller\\Controller' . $args['modulo'];
      $acao = $args['acao'];
      $parametro = null;
      if (isset($args['ref'])) {
        $parametro = $args['ref'];
      }
      $objeto = new $modulo($parametro);
      $objeto->$acao();
    }
    else {
      echo "<script>
        alert('Por favor, efetue login!');
        window.location.href = 'http://localhost/SISMAREPTB1/';
        </script>
      ";
    }
  });

  $app->run();
