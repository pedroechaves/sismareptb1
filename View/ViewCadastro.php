<?php
namespace SISMAREPTB1\View;

if(!isset($_SESSION))
{
    session_start();
}

if (!$_SESSION['login'] == true){
  echo "
    <script>
      window.location.href = 'http://localhost/SISMAREPTB1/'
    </script>
  ";
};

class ViewCadastro{
    public function __construct(){
    }

    public function cadastroPacientes($administradores)
    {
      $scripts =  array(
        "mascaras.js",
        "paciente.js"
      );
      $styles =  array(
        "estilo.css"
      );
      $titulo = "Cadastro";
      $pagina = "3";
      $super_usuario = $_SESSION['super_usuario'];
     
      $conteudo1 = '

        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Cadastro</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="/SISMAREPTB1/Inicio/agenda">Início</a></li>
                    <li class="breadcrumb-item active">Cadastro</li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <div class="card card-info">
            <div class="card-header">
            <h3 class="card-title">Cadastro de Pacientes</h3>
            </div>
            <div class="card-body">


            <form action=""  method="POST" name="formulario_cadastro" id="formulario_cadastro" data-toggle="validator"> 

                <div class="form-row">
                <div class="   col-md-8 ">
                    <label > Nome do Paciente:</label>
                    <div class="iconInput">
                    <i class="fa fa-user"></i>
                    <input type="text" name="paciente_nome" id="paciente_nome" class="form-control " placeholder="Digite o nome do paciente" required> 
                    <span class="help-block"></span>
                    </div>   
                </div>
                </div>
                <br>
                <div class="form-row">
                <div class="   col-md-8">
                    <label > Cpf:</label>
                    <div class="iconInput">
                    <i class="fa fa-address-card"></i>
                    <input type="text" name="paciente_cpf" id="paciente_cpf" class="form-control " placeholder="Digite o cpf do paciente" required> 
                    </div>   
                </div>
                </div>
                <br>
                <div class="form-row">
                <div class="form-group col-md-4 ">
                    <label for="inputCity">Telefone/Celular :</label>
                    <div class="iconInput"> 
                    <i class="fa fa-phone"></i>
                    <input type="text" name="paciente_telefone" id="paciente_telefone" class="form-control" placeholder="Digite o número de telefone" required>
                    </div>
                </div>
                </div>
                <div class="form-row">
                <div class="form-group col-md-3 ">
                    <label for="inputCity">Data de Nascimento:</label>
                    <div class="iconInput"> 
                    <i class="fa fa-calendar-week"></i>
                    <input type="date" name="paciente_data_nascimento" id="paciente_data_nascimento" class="form-control" required>
                    </div>
                </div>
                <div class="form-group col-md-2">
                    <label for="inputCity">Idade:</label>
                    <div class="iconInput"> 
                    <i class="fa fa-sort-numeric-up"></i>
                    <input type="text" name="paciente_idade" id="paciente_idade" class="form-control"   maxlength="5" disabled>
                    </div>
                </div>
                </div> 
                <div class="form-row">
                <div class="col-md-3">
                    <label > Data de Cadastro:</label>
                    <div class="iconInput">
                    <i class="fa fa-calendar-week"></i>
                    <input type="date" name="paciente_data_admissao" id="paciente_data_admissao" class="form-control " required> 
                    </div>   
                </div>
                </div><br>
                <div class="form-row">
                <div class="form-group col-md-6 ">
                    <label for="inputCity">Rua:</label>
                    <div class="iconInput"> 
                    <i class="fa fa-map-marker-alt"></i>
                    <input type="text" name="paciente_rua" id="paciente_rua" class="form-control" placeholder="Rua" required>
                    </div>
                </div>
                <div class="form-group col-md-2">
                    <label for="inputCity">Número:</label>
                    <div class="iconInput"> 
                    <i class="fa fa-sort-numeric-up"></i>
                    <input type="text" name="paciente_numero" id="paciente_numero" class="form-control" placeholder="Número"  maxlength="5" required>
                    </div>
                </div>
                </div>

                <div class="form-row">
                <div class="col-md-4 ">
                    <label> Bairro:</label> 
                    <div class="iconInput"> 
                    <i class="fa fa-map-marker-alt"></i>
                    <input type="text" name="paciente_bairro" id="paciente_bairro" class="form-control" placeholder="Digite o bairro" required>
                    </div>
                </div>
                </div>
                <br>
                <div class="form-row">
                <div class="form-group col-md-4 ">
                    <label for="inputCity">Cidade:</label>
                    <div class="iconInput"> 
                    <i class="fa fa-map-marker-alt"></i>
                    <input type="text" name="paciente_cidade" id="paciente_cidade" class="form-control" placeholder="Cidade" required>
                    </div>
                </div>
                <div class="form-group col-md-3">
                    <label for="inputCity">Estado:</label>
                    <select id="paciente_estado" name="paciente_estado" class="form-control" required>
                    <option selected disabled>Selecione...</option>
                    <option value="AC">Acre</option>
                    <option value="AL">Alagoas</option>
                    <option value="AP">Amapá</option>
                    <option value="AM">Amazonas</option>
                    <option value="BA">Bahia</option>
                    <option value="CE">Ceará</option>
                    <option value="DF">Distrito Federal</option>
                    <option value="ES">Espírito Santo</option>
                    <option value="GO">Goiás</option>
                    <option value="MA">Maranhão</option>
                    <option value="MT">Mato Grosso</option>
                    <option value="MS">Mato Grosso do Sul</option>
                    <option value="MG">Minas Gerais</option>
                    <option value="PA">Pará</option>
                    <option value="PB">Paraíba</option>
                    <option value="PR">Paraná</option>
                    <option value="PE">Pernambuco</option>
                    <option value="PI">Piauí</option>
                    <option value="RJ">Rio de Janeiro</option>
                    <option value="RN">Rio Grande do Norte</option>
                    <option value="RS">Rio Grande do Sul</option>
                    <option value="RO">Rondônia</option>
                    <option value="RR">Roraima</option>
                    <option value="SC">Santa Catarina</option>
                    <option value="SP">São Paulo</option>
                    <option value="SE">Sergipe</option>
                    <option value="TO">Tocantins</option>
                    <option value="EX">Estrangeiro</option>

                    </select>
                </div>
                </div>
                <div class="form-row">
                <div class=" col-md-6">
                    <label > Local de Tratamento:</label>
                    <div class="iconInput">
                    <i class="fa fa-map-marker-alt"></i>
                    <input type="text" name="paciente_local_tratamento" id="paciente_local_tratamento" class="form-control " placeholder="Digite o local de tratamento do paciente" required> 
                    </div>   
                </div>
                </div>
                <br>
                <div class="form-row">
                <div class="col-md-6">
                    <label> Observação:</label> 
                    <div class="iconInput"> 
                    <textarea name="paciente_observacao" id="paciente_observacao" class="col-md-12">
                        
                    </textarea>
                    
                    </div>
                </div>
                </div> 
                <br>  
                <div class="form-row ">
                <div>
                    <button type="submit" id="btn_salvar" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
                    <span class="help-block"></span>
                </div>
                </div>
            </form>            
            </div>
            <!-- /.card-body -->
        </div>
        <!-- /.content -->
        </div>
        ';
  
      $administrador = $_SESSION['nome'];
      include 'Templates/template.php';
    }
  }
  