$(function () {



	$(document).ready(function () {

		$("#paciente_telefone").mask("(99)999999999");
		$("#paciente_cpf").mask("999.999.999-99");

	});

	//Cadastrando um novo paciente
	$("#formulario_cadastro").submit(function () {

		$.ajax({
			type: "POST",
			url: BASE_URL + "Cadastro/ajax_cadastra_paciente",
			dataType: "json",
			data: $(this).serialize(),
			beforeSend: function () {
				$("#btn_salvar").html("<i class='fa fa-spinner'></i>&nbsp;&nbsp;Aguarde");
				$("#btn_salvar").prop('disabled', true);
			},
			success: function (response) {
				if (response["status"]) {
					Swal.fire(
						'Sucesso!',
						'Paciente cadastrado com sucesso!',
						'success'
					),
						$("#formulario_cadastro")[0].reset();
					$("#btn_salvar").html("<i class='fa fa-save'></i>&nbsp;&nbsp;Salvar");
					$("#btn_salvar").prop('disabled', false);

				} else {

					Swal.fire({
						icon: 'error',
						title: 'Erro ao realizar cadastro!',
						text: response["erros"]

					})
					$("#btn_salvar").html("<i class='fa fa-save'></i>&nbsp;&nbsp;Salvar");
					$("#btn_salvar").prop('disabled', false);
				}

			}
		})

		return false;
	});

	//Função para calcular data de nascimento
	function calculaIdade(dataNasc) {
		var dataAtual = new Date();
		var anoAtual = dataAtual.getFullYear();
		var anoNascParts = dataNasc.split('-');
		var diaNasc = anoNascParts[2];
		var mesNasc = anoNascParts[1];
		var anoNasc = anoNascParts[0];
		var idade = anoAtual - anoNasc;
		var mesAtual = dataAtual.getMonth() + 1;
		//Se mes atual for menor que o nascimento, nao fez aniversario ainda;  
		if (mesAtual < mesNasc) {
			idade--;
		} else {
			//Se estiver no mes do nascimento, verificar o dia
			if (mesAtual == mesNasc) {
				if (new Date().getDate() < diaNasc) {
					//Se a data atual for menor que o dia de nascimento ele ainda nao fez aniversario
					idade--;
				}
			}
		}
		if (idade < 0) {
			return 0;
		}
		else {
			return idade;
		}

	}

	$('#paciente_data_nascimento').blur(function () {
		var data_inicial = $('#paciente_data_nascimento').val();
		$("#paciente_idade").val(calculaIdade(data_inicial));

	});

	//Excluir um paciente
	//
	$(document).on('click', '.btn_excluir_paciente', function () {
		paciente_id = $(this);
		Swal.fire({
			title: "Atenção!",
			text: "Deseja excluir este paciente?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: "#d9534f",
			confirmButtonText: "Sim",
			cancelButtonText: 'Cancelar',
			closeOnConfirm: true,
			closeOnCancel: true,
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: BASE_URL + "ListagemPaciente/ajax_excluir_paciente",
					dataType: "json",
					data: { "paciente_numero": paciente_id.attr("paciente_numero_excluir") },
					success: function (response) {
						if (response["status"]) {
							const swalWithBootstrapButtons = Swal.mixin({
								customClass: {
									confirmButton: 'btn btn-success'
								},
								buttonsStyling: false
							})

							swalWithBootstrapButtons.fire({
								title: 'Sucesso!',
								text: "Paciente excluído com sucesso",
								icon: 'success',
								showCancelButton: false,
								confirmButtonText: 'Confirmar',
								closeOnClickOutside: false,
								closeOnEsc: false,
								allowOutsideClick: false,
								reverseButtons: true
							}).then((result) => {
								if (result.value) {
									window.location.replace(BASE_URL + "ListagemPaciente/pacientes");
								}
							})

						}
						else {
							Swal.fire({
								icon: 'error',
								title: 'Erro ao excluir!',
								text: response["listaErros"]

							})
						}


					}
				})
			}
		})
		return false;
	});



	//Atualizando um paciente
	$("#formulario_editar_paciente").submit(function () {

		$.ajax({
			type: "POST",
			url: BASE_URL + "Pacientes/ajax_atualiza_paciente",
			dataType: "json",
			data: $(this).serialize(),
			beforeSend: function () {
				$("#btn_editar").html("<i class='fa fa-spinner'></i>&nbsp;&nbsp;Aguarde");
				$("#btn_editar").prop('disabled', true);
			},
			success: function (response) {
				if (response["status"]) {


					Swal.fire(
						'Sucesso!',
						'Dados atualizados com sucesso!',
						'success'
					)
					$("#btn_editar").html("<i class='fa fa-edit'></i>&nbsp;&nbsp;Editar");
					$("#btn_editar").prop('disabled', false);


				} else {

					Swal.fire({
						icon: 'error',
						title: 'Erro!',
						text: response["listaErros"]

					})
					$("#btn_editar").html("<i class='fa fa-edit'></i>&nbsp;&nbsp;Editar");
					$("#btn_editar").prop('disabled', false);
				}

			}
		})

		return false;
	});











});