<?php

namespace SISMAREPTB1\Controller;

use SISMAREPTB1\Model\ModelAdministrador;
use SISMAREPTB1\View\ViewAdministrador;

class ControllerAdministrador
{
  public function __construct($parametro = null)
  {
  }

  public function loggout()
  {
    session_destroy();
    echo "
      <script>
          window.location.href = 'http://localhost/SISMAREPTB1/'
        </script>
      ";
  }

  public function login()
  {
    $email = filter_input(INPUT_POST, 'usuario_email');
    $senha = filter_input(INPUT_POST, 'usuario_senha');
    $ModelAdministrador = new ModelAdministrador(null);

    $ViewAdministrador = new ViewAdministrador();

    $data = $ModelAdministrador->login($email);

    if ($data != false) {

      if (password_verify($senha, $data[0]['administrador_senha'])) {
        $_SESSION['login'] = true;
        $_SESSION['nome'] = $data[0]['administrador_nome'];
        $_SESSION['id'] = $data[0]['administrador_id'];
        $_SESSION['super_usuario'] = $data[0]['administrador_super_usuario'];
      }
    }
  }

  public function verificaLogin()
  {
    if (isset($_SESSION['login'])) {
      return true;
    } else {
      return false;
    }
  }

  public function administradores()
  {
    $administradores = new ModelAdministrador();
    $administradores->setId(null);
    $administrador = $administradores->listaAdministradores();
    $ViewEsqueceuSenha = new ViewAdministrador();
    $ViewEsqueceuSenha->listarAdministradores($administrador);
  }


}
