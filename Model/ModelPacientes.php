<?php
namespace SISMAREPTB1\Model;

use PDO;
use PDOException;

  class ModelPacientes
  {
    private $id;
    private $nome;
    private $cpf;
    private $telefone;
    private $dataNascimento;
    private $rua;
    private $numero;
    private $bairro;
    private $cidade;
    private $estado;
    private $localTratamento;
    private $observacao;
    private $dataAdmissao;

    public function __construct($id = null) {
      $this->id = $id;
    }

    public function getId(){
      return $this->id;
    }

    public function setId($id){
      $this->id = $id;
    }

    public function getNome(){
      return $this->nome;
    }

    public function setNome($nome){
      $this->nome = $nome;
    }

	  public function getCpf() {
	  	return $this->cpf;
	  }

	  public function setCpf($cpf) {
	  	$this->cpf = $cpf;
	  }

	  public function getTelefone() {
	  	return $this->telefone;
	  }

	  public function setTelefone($telefone) {
	  	$this->telefone = $telefone;
	  }

	  public function getDataNascimento() {
	  	return $this->dataNascimento;
	  }

	  public function setDataNascimento($dataNascimento) {
	  	$this->dataNascimento = $dataNascimento;
	  }

	  public function getRua() {
	  	return $this->rua;
	  }

	  public function setRua($rua = null) {
	  	$this->rua = $rua;
	  }

	  public function getNumero() {
	  	return $this->numero;
	  }

	  public function setNumero($numero) {
	  	$this->numero = $numero;
	  }

	  public function getBairro() {
	  	return $this->bairro;
	  }

	  public function setBairro($bairro) {
	  	$this->bairro = $bairro;
	  }

	  public function getCidade() {
	  	return $this->cidade;
	  }

	  public function setCidade($cidade ) {
	  	$this->cidade = $cidade;
	  }

	  public function getEstado() {
	  	return $this->estado;
	  }

	  public function setEstado($estado ) {
	  	$this->estado = $estado;
	  }

	  public function getLocalTratamento() {
	  	return $this->localTratamento;
	  }

	  public function setLocalTratamento($localTratamento) {
	  	$this->localTratamento = $localTratamento;
	  }

	  public function getObservacao() {
	  	return $this->observacao;
	  }

	  public function setObservacao($observacao) {
	  	$this->observacao = $observacao;
	  }

	  public function getDataAdmissao() {
	  	return $this->dataAdmissao;
	  }

	  public function setDataAdmissao($dataAdmissao) {
	  	$this->dataAdmissao = $dataAdmissao;
    }
    
     //Verifica se já possui algum paciente cadastrado com o CPF informado
  public function Verificar_Duplicidade($valor, $id)
  {
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
      if (!empty($id)) {  
        $sql = $con->query("select * from pacientes where paciente_cpf='$valor' and paciente_cpf<>'$id'")->fetchAll();
      } else {
        $sql = $con->query("select * from pacientes where paciente_cpf='$valor'")->fetchAll();
      }
      return count($sql);
    } catch (PDOException $pdoex) {
      return false;
    }
  }

    //Inserindo um novo paciente
    public function cadastraPaciente()
    {
      $user = 'root';
      $pass = '';
      try {
        $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
        $sql = 'insert into pacientes (paciente_nome, paciente_cpf, paciente_telefone, paciente_data_nascimento, paciente_rua, paciente_numero, paciente_bairro, paciente_cidade, paciente_estado, paciente_local_tratamento, paciente_observacao, paciente_data_admissao) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);';
        $pre = $con->prepare($sql);        
        $pre->bindValue(1, $this->nome);
        $pre->bindValue(2, $this->cpf);
        $pre->bindValue(3, $this->telefone);
        $pre->bindValue(4, $this->dataNascimento);
        $pre->bindValue(5, $this->rua);
        $pre->bindValue(6, $this->numero);
        $pre->bindValue(7, $this->bairro);
        $pre->bindValue(8, $this->cidade);
        $pre->bindValue(9, $this->estado);
        $pre->bindValue(10, $this->localTratamento);
        $pre->bindValue(11, $this->observacao);
        $pre->bindValue(12, $this->dataAdmissao);
        if ($pre->execute()) {
          return true;
        } else {
          return false;
        }
      } catch (PDOException $pdoex) {
        return false;
      }
    }
  
    //Retorna os pacientes cadastrados
    public function listaPacientes()
    {
      $user = 'root';
      $pass = '';
      try {
        $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
        if (!empty($this->id)) {
          $sql="select * from pacientes where paciente_id ='$this->id'";
        } else {
          $sql = 'select * from pacientes order by paciente_id  desc';
        }
  
        if ($data = $con->query($sql)) {
          return $data->fetchAll(PDO::FETCH_ASSOC);
        } else {
          return false;
        }
      } catch (PDOException $pdoex) {
        return false;
      }
    }  
  
    //Exclui um administrador do banco
    public function excluiPacientes()
    {
      $user = 'root';
      $pass = '';
      try {
        $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
        $sql = 'DELETE FROM `pacientes` WHERE paciente_id = ' . $this->id . ';';
        if ($data = $con->query($sql)) {
          return true;
        } else {
          return false;
        }
      } catch (PDOException $pdoex) {
        return false;
      }
    }



     //Editando um paciente
     public function editaPaciente()
     {
       $user = 'root';
       $pass = '';
       try {
         $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
         $sql = 'update pacientes set paciente_nome=?, paciente_cpf=?, paciente_telefone=?, paciente_data_nascimento=?, paciente_rua=?, paciente_numero=?, paciente_bairro=?, paciente_cidade=?, paciente_estado=?, paciente_local_tratamento=?, paciente_observacao=?, paciente_data_admissao=? where paciente_id= ' . $this->id .';;';
         $pre = $con->prepare($sql);        
         $pre->bindValue(1, $this->nome);
         $pre->bindValue(2, $this->cpf);
         $pre->bindValue(3, $this->telefone);
         $pre->bindValue(4, $this->dataNascimento);
         $pre->bindValue(5, $this->rua);
         $pre->bindValue(6, $this->numero);
         $pre->bindValue(7, $this->bairro);
         $pre->bindValue(8, $this->cidade);
         $pre->bindValue(9, $this->estado);
         $pre->bindValue(10, $this->localTratamento);
         $pre->bindValue(11, $this->observacao);
         $pre->bindValue(12, $this->dataAdmissao);
         if ($pre->execute()) {
           return true;
         } else {
           return false;
         }
       } catch (PDOException $pdoex) {
         return false;
       }
     }
    
  }
 ?>
