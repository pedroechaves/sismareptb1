<?php

namespace SISMAREPTB1\Model;

use PDO;
use PDOException;

class ModelAdministrador
{
  private $id;
  private $email;
  private $senha;
  private $nome;
  private $telefone;
  private $superusuario;


  public function __construct($id = null)
  {
    $this->id = $id;
  }

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }

  public function getSenha()
  {
    return $this->senha;
  }

  public function setSenha($senha)
  {
    $this->senha = $senha;
  }

  public function getNome()
  {
    return $this->nome;
  }

  public function setNome($nome)
  {
    $this->nome = $nome;
  }

  public function getTelefone()
  {
    return $this->telefone;
  }

  public function setTelefone($telefone)
  {
    $this->telefone = $telefone;
  }

  public function getSuperUsuario()
  {
    return $this->superusuario;
  }

  public function setSuperUsuario($superusuario)
  {
    $this->superusuario = $superusuario;
  }




  //Inserindo um novo administrador
  public function cadastraAdministrador()
  {
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
      $sql = 'insert into administradores (administrador_email, administrador_senha,administrador_nome,administrador_telefone,administrador_super_usuario) values (?, ?, ?, ?, ?);';
      $pre = $con->prepare($sql);
      $pre->bindValue(1, $this->email);
      $pre->bindValue(2, $this->senha);
      $pre->bindValue(3, $this->nome);
      $pre->bindValue(4, $this->telefone);
      $pre->bindValue(5, $this->superusuario);
      if ($pre->execute()) {
        return true;
      } else {
        return false;
      }
    } catch (PDOException $pdoex) {
      return false;
    }
  }

  /*****************************************************Fazendo login*******************/
  public function login($email)
  {
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
      $sql = "select administrador_id ,administrador_email,administrador_super_usuario,administrador_nome, administrador_senha from administradores where administrador_email='$email'";
      if ($data = $con->query($sql)) {
        return $data->fetchAll(PDO::FETCH_ASSOC);
      } else {
        return false;
      }
    } catch (PDOException $pdoex) {
      return false;
    }
  }


  //Retorna os administradores cadastrados
  //
  public function listaAdministradores()
  {
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
      if (!empty($this->id)) {
        $sql="select * from administradores where administrador_id='$this->id'";
      } else {
        $sql = 'select * from administradores order by administrador_id desc';
      }

      if ($data = $con->query($sql)) {
        return $data->fetchAll(PDO::FETCH_ASSOC);
      } else {
        return false;
      }
    } catch (PDOException $pdoex) {
      return false;
    }
  }



  //
  //Exclui um administrador do banco
  //
  public function excluiAdministrador()
  {
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
      $sql = 'DELETE FROM `administradores` WHERE administrador_id = ' . $this->id . ';';
      if ($data = $con->query($sql)) {
        return true;
      } else {
        return false;
      }
    } catch (PDOException $pdoex) {
      return false;
    }
  }



  //Verifica se já possui algum administrador cadastrado com o email informado
  //
  public function Verificar_Duplicidade($valor, $id)
  {
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
      if (!empty($id)) {  
        $sql = $con->query("select * from administradores where administrador_email='$valor' and administrador_id<>'$id'")->fetchAll();
      } else {
        $sql = $con->query("select * from administradores where administrador_email='$valor'")->fetchAll();
      }
      return count($sql);
    } catch (PDOException $pdoex) {
      return false;
    }
  }

  //
	//Atualiza os dados do usuario que faz login
	
	public function atualizaAdministrador()
	{
    $user = 'root';
      $pass = '';
      try {
        $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
        $sql = 'update administradores set administrador_nome = ?, administrador_telefone = ? where administrador_id= ' . $this->id .';';
        $pre = $con->prepare($sql);
        $pre->bindValue(1, $this->nome);
        $pre->bindValue(2, $this->telefone);
        if ($pre->execute()){
          return true;
        }else{
         return false;
        }
      }catch(PDOException $pdoex){
        return true;
      }
  }


  //
	//Atualiza os dados do usuario que faz login
	
	public function atualizaSenhaAdmin($flag)
	{
    $user = 'root';
      $pass = '';
      try {
        $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
        if (!empty($flag)) {
          $sql = 'update administradores set administrador_senha = ? where administrador_id= ' . $this->email .';';
        }
        else
        {
          $sql = 'update administradores set administrador_senha = ? where administrador_id= ' . $this->id .';';
        }
        
        $pre = $con->prepare($sql);
        $pre->bindValue(1, $this->senha);
        if ($pre->execute()){
          return true;
        }else{
         return false;
        }
      }catch(PDOException $pdoex){
        return true;
      }
  }
}
