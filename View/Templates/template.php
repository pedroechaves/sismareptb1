<!DOCTYPE html>
<html lang="pt-br">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= $titulo ?></title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="../Public/css/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="../Public/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../Public/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="shortcut icon" href="../Public/img/logo2.svg" type="image/x-icon" />
  <?php if (isset($styles)) {

    foreach ($styles as $style_name) {
      $href = "../Public/css/" . $style_name; ?>
      <link href="<?= $href ?>" rel="stylesheet">

  <?php }
  }
  ?>


</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
  <div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
      </ul>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">

      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <a href="/SISMAREPTB1/Inicio/agenda" class="brand-link">
        <img src="../Public/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Administrador</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
          <div class="image">
            <img src="../Public/img/avatar2.png" class="img-circle elevation-4" alt="User Image">

          </div>
          <div class="info">
            <a href="/SISMAREPTB1/MeusDados/ver" class="d-block" id="name"><?= $administrador ?></a>
          </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
          <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
            <li class="nav-item">
              <?php
              if ($pagina == 1) {
              ?>
                <a href="/SISMAREPTB1/Inicio/agenda" class="nav-link active">
                <?php } else {
                ?>
                  <a href="/SISMAREPTB1/Inicio/agenda" class="nav-link">
                  <?php }
                  ?>
                  <i class="nav-icon fas fa-home"></i>
                  <p>
                    Inicio
                  </p>
                  </a>
            </li>

            <li class="nav-item">
              <?php
              if ($pagina == 2) {
              ?>
                <a href="/SISMAREPTB1/ListagemPaciente/pacientes" class="nav-link active">
                <?php } else {
                ?>
                  <a href="/SISMAREPTB1/ListagemPaciente/pacientes" class="nav-link">
                  <?php }
                  ?>
                  <i class="nav-icon fas fa-user-md"></i>
                  <p>
                    Pacientes
                  </p>
                  </a>
            </li>
            <li class="nav-item">
              <?php
              if ($pagina == 3) {
              ?>
                <a href="/SISMAREPTB1/Cadastro/pacientes" class="nav-link active">
                <?php } else {
                ?>
                  <a href="/SISMAREPTB1/Cadastro/pacientes" class="nav-link">
                  <?php }
                  ?>
                  <i class="nav-icon fas fa-plus-square"></i>
                  <p>
                    Cadastro Paciente
                  </p>
                  </a>
            </li>

            <?php
            if ($super_usuario == 1) {
            ?>
              <li class="nav-item">
                <?php
                if ($pagina == 4) {
                ?>
                  <a href="/SISMAREPTB1/Administrador/administradores" class="nav-link active">
                  <?php } else {
                  ?>
                    <a href="/SISMAREPTB1/Administrador/administradores" class="nav-link">
                    <?php }
                    ?>
                    <i class="nav-icon fas fa-user-shield"></i>
                    <p>
                      Administradores
                    </p>
                    </a>
              </li>
            <?php }
            ?>
            <li class="nav-item">
              <a href="/SISMAREPTB1/Administrador/loggout" class="nav-link">
                <i class="nav-icon fas fa-power-off"></i>
                <p>
                  Sair
                </p>
              </a>
            </li>

          </ul>

        </nav>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Conteúdo -->

    <?php
    echo $conteudo1;
    ?>



    <!-- /.Conteúdo -->

    <footer class="main-footer">
      <strong>Copyright &copy; 2020 <a href="#" target="_blanck">SISMAREP</a>.</strong>
      Todos os direitos reservados
      <div class="float-right d-none d-sm-inline-block">
        <b>Versão</b> 1.0.0
      </div>
    </footer>

    <!-- jQuery -->
    <script src="../Public/js/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="../Public/js/bootstrap.bundle.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="../Public/js/jquery.overlayScrollbars.min.js"></script>
    <!-- Hamburguer -->
    <script src="../Public/js/adminlte.js"></script>

    <!-- Base -->
    <script src="../Public/js/base.js"></script>
    <!-- Alerts -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script type="text/javascript" src="//assets.locaweb.com.br/locastyle/2.0.6/javascripts/locastyle.js"></script>

    <?php if (isset($scripts)) {
      foreach ($scripts as $script_name) {
        $src = "../Public/js/" . $script_name; ?>
        <script src="<?= $src ?>"></script>
    <?php }
    } ?>

</body>

</html>