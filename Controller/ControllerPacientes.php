<?php
namespace SismarepTB1\Controller;
use SISMAREPTB1\Model\ModelPacientes;
use SISMAREPTB1\View\ViewPacientes;

 class ControllerPacientes{
    public function __construct($param=null){
    }

    public function dadospaciente(){
      $paciente = new ModelPacientes();
      $paciente->setId($_GET['id']);
      $dados = $paciente->listaPacientes()[0];
      $ViewPacientes = new ViewPacientes();
      $ViewPacientes->listardados($dados);
    }

    //Função para editar
  public function ajax_atualiza_paciente()
  {

    $json = array();
    $json["status"] = 1;
    $json["erros"] = array();

    $pacientes = new ModelPacientes();
    if ($pacientes->Verificar_Duplicidade($_POST['paciente_cpf'], $_POST['paciente_numero_cadastro']) > 0) {
      $json["erros"] = "Já existe um paciente cadastrado com esse cpf!";
      $json["status"] = 0;
      echo json_encode($json);
      exit;
    }
    
      $pacientes->setId($_POST['paciente_numero_cadastro']);
       $pacientes->setNome($_POST['paciente_nome']);
       $pacientes->setCpf($_POST['paciente_cpf']);
       $pacientes->setTelefone($_POST['paciente_telefone']);
       $pacientes->setDataNascimento($_POST['paciente_data_nascimento']);
       $pacientes->setRua($_POST['paciente_rua']);
       $pacientes->setNumero($_POST['paciente_numero']);
       $pacientes->setBairro($_POST['paciente_bairro']);
       $pacientes->setCidade($_POST['paciente_cidade']);
       $pacientes->setEstado($_POST['paciente_estado']);
       $pacientes->setLocalTratamento($_POST['paciente_local_tratamento']);
       $pacientes->setObservacao($_POST['paciente_observacao']);
       $pacientes->setDataAdmissao($_POST['paciente_data_admissao']);
    if ($pacientes->editaPaciente()) {
      $json["status"] = 1;
      echo json_encode($json);
    } else {
      $json["erros"] = "Contate o administrador!";
      $json["status"] = 0;
      echo json_encode($json);
      exit;
    }
  }
   

   
  }
