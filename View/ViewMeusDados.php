<?php

namespace SISMAREPTB1\View;

if (!isset($_SESSION)) {
    session_start();
}

if (!$_SESSION['login'] == true) {
    echo "
    <script>
      window.location.href = 'http://localhost/SISMAREPTB1/'
    </script>
  ";
};

class ViewMeusDados
{
    public function __construct()
    {
    }

    public function listardados($dados)
    {
        $scripts =  array(
            "mascaras.js",
            "administradores.js"
        );
        $styles =  array(
            "estilo.css"
        );
        $titulo = "Meus Dados";
        $pagina = "3";
        $super_usuario = $_SESSION['super_usuario'];
        $administrador = $_SESSION['nome'];
        $conteudo1 = '
        <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/SISMAREPTB1/Inicio/agenda">Início</a></li>
                        <li class="breadcrumb-item active">Meus Dados</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title">Meus Dados</h3>
        </div>
        <div class="card-body">
            <form action="" method="POST" name="form_altera_administradores" id="form_altera_administradores" data-toggle="validator">
                <div class="form-row">
                    <div class="col-md-8">
                        <label> Nome:</label>
                        <div class="iconInput">
                            <i class="fa fa-user-shield"></i>
                            <input type="text" name="administrador_nome" id="administrador_nome" class="form-control " placeholder="Digite seu nome" value="' . $dados['administrador_nome'] . '" required>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-row">
                    <div class="col-md-8">
                        <label> Email:</label>
                        <div class="iconInput">
                            <i class="fa fa-envelope"></i>
                            <input type="email" name="administrador_email" id="administrador_email" class="form-control " value="' . $dados['administrador_email'] . '" disabled>
                        </div>
                    </div>
                </div>
                <br>
                <div class="form-row">
                    <div class="col-md-8">
                        <label> Telefone:</label>
                        <div class="iconInput">
                            <i class="fa fa-phone"></i>
                            <input type="text" name="administrador_telefone" id="administrador_telefone" class="form-control " placeholder="Digite seu telefone" value="' . $dados['administrador_telefone'] . '" required>
                        </div>
                    </div>
                </div>

                <br>

                <div class="form-row ">
                    <div class="col-md-6">
                        <button type="submit" id="btn_altera_administrador" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
                        <span class="help-block"></span>
                    </div>
                </div>


            </form>
            <br>

            <div class="form-row ">
                <div>
                    <button class="btn btn-primary" id="altera_senha"><i class="fa fa-key"></i> Alterar Senha</button>
                    <span class="help-block"></span>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>

</div>

<div class="modal" tabindex="-1" role="dialog" id="modal_altera_senha">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Alterar Senha</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="formulario_altera_senha" name="formulario_altera_senha">
                    <div class="form-row">
                        <div class="form-group col-md-8 ">
                            <label for="inputCity">Senha antiga :</label>
                            <div class="iconInput">
                                <i class="fa fa-key"></i>
                                <input type="password" name="administradorSenhaAntiga" id="administradorSenhaAntiga" class="form-control" placeholder="Digite sua senha atual" required>

                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-8 ">
                            <label for="inputCity">Nova Senha :</label>
                            <div class="iconInput">
                                <i class="fa fa-key"></i>
                                <input type="password" name="administrador_senha" id="administrador_senha" class="form-control" placeholder="Digite sua nova senha" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-8 ">
                            <label for="inputCity">Confirmar Senha :</label>
                            <div class="iconInput">
                                <i class="fa fa-key"></i>
                                <input type="password" name="administradorConfirmaSenha" id="administradorConfirmaSenha" class="form-control" placeholder="Confirme sua nova senha" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-row ">
                        <div>
                            <button type="submit" id="btn_salva_senha" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Salvar</button>
                            <span class="help-block"></span>
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
        ';
        include 'Templates/template.php';
    }
}
