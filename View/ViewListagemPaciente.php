<?php

namespace SISMAREPTB1\View;

if (!isset($_SESSION)) {
    session_start();
}

if (!$_SESSION['login'] == true) {
    echo "
    <script>
      window.location.href = 'http://localhost/SISMAREPTB1/'
    </script>
  ";
};
class ViewListagemPaciente
{
    public function __construct()
    {
    }

    public function listarpacientes($pacientes)
    {
        $scripts =  array(
            "jquery.dataTables.min.js",
            "dataTables.bootstrap4.min.js",
            "dataTables.responsive.min.js",
            "responsive.bootstrap4.min.js",
            "datatables.js",
            "paciente.js"
        );
        $styles =  array(
            "dataTables.bootstrap4.min.css",
            "responsive.bootstrap4.min.css",
        );
        $titulo = "Listagem de Pacientes";
        $pagina = "2";
        $super_usuario = $_SESSION['super_usuario'];
        $listapacientes = '';
        foreach ($pacientes as $paciente) {
            if ($paciente['paciente_data_nascimento']!='0000-00-00') {
                $data_nascimento = date("d/m/Y", strtotime($paciente['paciente_data_nascimento']));
              }
              else
              {
               $data_nascimento = null;
             }
             
             $listapacientes = $listapacientes. "
             <tr>
             <td>".$paciente['paciente_id']."</td>
             <td>".$paciente['paciente_nome']."</td>
             <td>".$paciente['paciente_cpf']."</td>
             <td>".$data_nascimento."</td>
             <td><a href='/SISMAREPTB1/Pacientes/dadospaciente?id=".$paciente['paciente_id']."'><button class='btn btn-primary'><i class='fa fa-eye'></i></button></a>
             <a href=''><button class='btn btn-danger btn_excluir_paciente' paciente_numero_excluir='".$paciente['paciente_id']."'><i class='fa fa-trash-alt'></i></button></a>
             </td>
             </tr>

             ";
        }
        $conteudo1 = '
        <div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Pacientes</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/SISMAREPTB1/Inicio/agenda">Início</a></li>
            <li class="breadcrumb-item active">Pacientes</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">


          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Listagem de Pacientes Cadastrados</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="teste" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Cód.</th>
                    <th>Nome</th>
                    <th>Cpf</th>
                    <th>Data Nas.</th>
                    <th>Ações</th>
                  </tr>
                </thead>
                <tbody>
                  '.$listapacientes.'

              </tbody>

            </table>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
  </div>
  <!-- /.container-fluid -->
</section>

</div>
        ';

        $administrador = $_SESSION['nome'];
        include 'Templates/template.php';
    }
}
