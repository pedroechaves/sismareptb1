<?php
  namespace SISMAREPTB1\Controller;

  use SISMAREPTB1\Model\ModelAgendamentos;
  use SISMAREPTB1\Model\ModelPacientes;
  use SISMAREPTB1\View\ViewInicio;

  class ControllerInicio{
    public function __construct($parametro=null){
    }

    public function agenda(){
      $agendamentos = new ModelAgendamentos();
      $pacientes = new ModelPacientes();
      $pacientes->setId(null);
      $paciente = $pacientes->listaPacientes();
      $medicos=$agendamentos->listaMedicos();
      $visaoInicio = new ViewInicio();
      $visaoInicio->listar($medicos,$paciente);
    }

    public function listaHorarios()
    {
      
      $json = array();
      $json["status"] = 1;
      if (empty($_POST['diaAtual'])) {
        $dia = null;
      } else {
        $dia = $_POST['diaAtual'];
      }
      $agendamento = new ModelAgendamentos();
      $agendamento->setData($dia);
      $agendamentos = $agendamento->listaAgendamentos();
      if ($agendamentos!=false) {
        $horarios = '';
        $aux = 8;
  
        foreach ($agendamentos as $agenda) {
          $hora =  substr($agenda['agendamento_hora'], 0, -3);
          if ($hora[0] == 0) {
            $hora =  substr($agenda['agendamento_hora'], 1, -3);
          }
  
          for ($cont = $aux; $cont <= 18; $cont++) {
            if ($aux.':00' == $hora) {
              $horarios = $horarios . "
                  <tr style='background-color:#FF6347;'>
                  <td>" . $hora . "</td>
                  <td>" . $agenda['paciente_nome'] . "</td>
                  <td>" . $agenda['medico_nome'] . "</td>
                  <td><button class='btn btn-primary btn_detalhes_agendamento' agendamento_detalhes=" . $agenda['agendamento_id'] . "><i class='fa fa-info-circle'></i></button>
                  <button class='btn btn-danger btn_cancela_agendamento' agendamento_numero=" . $agenda['agendamento_id'] . "><i class='fa fa-ban'></i></button>
                  </td>
                  
                </tr>
                  ";
              $aux++;
              break;
            } else {
              $horarios = $horarios . '
              <tr style="background-color:#00FF7F;">
              <td>' . $cont . ':00</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
              ';
              $aux++;
            }
          }
        }
        if ($aux < 19) {
          for ($cont = $aux; $cont <= 18; $cont++) {
  
            $horarios = $horarios . '
              <tr style="background-color:#00FF7F;">
              <td>' . $cont . ':00</td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
              ';
            $aux = $cont;
          }
        }
        $json["horarios"] = $horarios;
      } else {
        $aux2 = 8;
        $horarios = '';
        for ($cont = $aux2; $cont <= 18; $cont++) {
  
          $horarios = $horarios . '
            <tr style="background-color:#00FF7F;">
            <td>' . $cont . ':00</td>
            <td></td>
            <td></td>
            <td></td>
          </tr>
            ';
          $aux2 = $cont;
        }
        $json["horarios"] = $horarios;
      }
      echo json_encode($json);
    }

  public function verificaHorario()
	{
		$json = array();
		$json["status"] = 1;
		$agendamento = new ModelAgendamentos();
		if ($agendamento->Verificar_Disponibilidade($_POST['dataselecionada'], $_POST['horaselecionada'])>0) {
			$json["status"] = 0;
		}

		echo json_encode($json);
  }
  
  //Função para o cadastro de um agendamento
	public function ajax_cadastra_agendamento()
	{
	
		$json = array();
		$json["status"] = 1;
		$json["listaErros"] = array();
    $json["datainformada"] ='';

		$agendamento = new ModelAgendamentos();
		if ($agendamento->Verificar_Disponibilidade($_POST['agendamento_data'], $_POST['agendamento_hora'])>0) {
			$json["listaErros"] = "Horário não está disponível!";
			$json["status"] = 0;
			echo json_encode($json);
			exit;
		}
		$datainformada = $_POST['agendamento_data'];
		$datahoje = date("Y-m-d");

		if (strtotime($datainformada) < strtotime($datahoje)) {
			$json["listaErros"] = "Data informada é menor que a data atual!";
			$json["status"] = 0;
			echo json_encode($json);
			exit;
    }
    
    $agendamento->setData($_POST['agendamento_data']);
    $agendamento->setPaciente($_POST['paciente_id']);
    $agendamento->setMedico($_POST['medico_id']);
    $agendamento->setHora($_POST['agendamento_hora']);
    $agendamento->setSintomas($_POST['agendamento_sintomas']);
    $agendamento->setEmail($_POST['paciente_email']);
    $agendamento->setPreco($_POST['agendamento_preco']);
    $agendamento->insereAgendamento();
    $json['datainformada'] = $_POST['agendamento_data'];
		echo json_encode($json);
  }
  

  public function listaAgendamento()
	{
		
		$json = array();
		$json["status"] = 1;
		$agendamento = new ModelAgendamentos();	
    $agendamentoId = $_POST['agendamento_detalhes'];
    $agendamento->setId($agendamentoId);
    $agendamentos = $agendamento->listaAgendamentosId()[0];
    
		$json['informacoes'] = '
		<div class="item"><span class="price">' . $agendamentos['paciente_nome'] . '</span>
                                         <p class="item-name">Paciente</p>
                                     </div>
                                     <div class="item"><span class="price">' . $agendamentos['paciente_telefone'] . '</span>
                                         <p class="item-name">Contato</p>
                                     </div>
                                     <div class="item"><span class="price">' . $agendamentos['medico_nome'] . '</span>
                                         <p class="item-name">Médico</p>
                                     </div>
                                     <div class="item"><span class="price">' . date("d/m/Y", strtotime($agendamentos['agendamento_data'])) . ' - ' . $agendamentos['agendamento_hora'] . '</span>
                                         <p class="item-name">Data/Hora</p>
                                     </div>
                                     <div class="item"><span class="price">R$' . $agendamentos['agendamento_preco'] . '</span>
                                         <p class="item-name">Valor</p>
									 </div>
									 <div class="item"><span class="price"></span>
				<p class="item-name">Sintomas</p>
				<p class="item-description">' . $agendamentos['agendamento_sintomas'] . '</p>
			</div>

                                     <div class="total"><span class="price" id="total"></span></div>

		';

		echo json_encode($json);
  }
  
  public function ajax_cancela_agendamento()
	{

		/*if (!$this->input->is_ajax_request()) {
			exit("Nenhum acesso de script direto permitido!");
		}*/

		$json = array();
		$json["status"] = 1;
		$json["listaErros"] = array();
    $agendamento = new ModelAgendamentos();	
    $agendamentoId = $_POST['agendamento_numero'];
    $agendamento->setId($agendamentoId);
    $agendamento->excluiAgendamento();
		
		echo json_encode($json);
	}


    
  }
 ?>
