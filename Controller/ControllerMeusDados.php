<?php

namespace SISMAREPTB1\Controller;

use SISMAREPTB1\Model\ModelAdministrador;
use SISMAREPTB1\View\ViewMeusDados;

class ControllerMeusDados
{
    public function __construct($param = null)
    {
    }

    public function ver()
    {
        if (!isset($_SESSION)) {
            session_start();
        }
        $administrador = new ModelAdministrador();
        $administrador->setId($_SESSION['id']);
        $dados = $administrador->listaAdministradores()[0];
        $ViewDados = new ViewMeusDados();
        $ViewDados->listardados($dados);
    }
}
