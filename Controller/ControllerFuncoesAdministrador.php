<?php
  namespace SISMAREPTB1\Controller;

  use SISMAREPTB1\Model\ModelAdministrador;

  class ControllerFuncoesAdministrador{
    public function __construct($parametro=null){
    }

   
  
  public function ajax_remove_administrador()
  {

    $json = array();
    $json["status"] = 1;
    $json["erros"] = array();
    $administradores = new ModelAdministrador();
    $administradores->setId($_POST['administrador']);
    if ($administradores->excluiAdministrador()) {
      $json["status"] = 1;
    } else {
      $json["erros"] = "Contate o administrador!";
      $json["status"] = 0;
      echo json_encode($json);
      exit;
    }


    echo json_encode($json);
  }


   //Função para cadastrar um administrador
   public function ajax_cadastra_administrador()
   {
 
     $json = array();
     $json["status"] = 1;
     $json["erros"] = array();
 
     if ($_POST['administrador_senha'] != $_POST['confirmaSenha']) {
       $json["erros"] = "Senha e confirmação são diferentes!";
       $json["status"] = 0;
       echo json_encode($json);
       exit;
     }
 
 
     $administrador = new ModelAdministrador();
     if ($administrador->Verificar_Duplicidade($_POST['administrador_email'], null) > 0) {
       $json["erros"] = "Já existe um administrador cadastrado com esse email!";
       $json["status"] = 0;
       echo json_encode($json);
       exit;
     }
     $administrador->setEmail($_POST['administrador_email']);
     $administrador->setSenha(password_hash($_POST['administrador_senha'], PASSWORD_DEFAULT));
     $administrador->setNome($_POST['administrador_nome']);
     $administrador->setTelefone($_POST['administrador_telefone']);
     $administrador->setSuperUsuario($_POST['administrador_super_usuario']);
     if ($administrador->cadastraAdministrador()) {
       $json["status"] = 1;
       echo json_encode($json);
     } else {
       $json["erros"] = "Contate o administrador!";
       $json["status"] = 0;
       echo json_encode($json);
       exit;
     }
   }


    //Função para atualizar dados de um administrador
    public function ajax_edita_administrador()
    {
  
      $json = array();
      $json["status"] = 1;
      $json["erros"] = array();
      if (!isset($_SESSION)) {
        session_start();
     }
      
      $administrador = new ModelAdministrador();
      $administrador->setId($_SESSION['id']);
      $administrador->setNome($_POST['administrador_nome']);
      $administrador->setTelefone($_POST['administrador_telefone']);
      if ($administrador->atualizaAdministrador()) {
        $json["status"] = 1;
        echo json_encode($json);
      } else {
        $json["erros"] = "Contate o administrador!";
        $json["status"] = 0;
        echo json_encode($json);
        exit;
      }
    }


     //Função para atualizar senha de um administrador
     public function ajax_edita_senha()
     {
   
       $json = array();
       $json["status"] = 1;
       $json["erros"] = array();
       if (!isset($_SESSION)) {
         session_start();
      }
       
       $administrador = new ModelAdministrador();
      $administrador->setId($_SESSION['id']);
      $dados = $administrador->listaAdministradores()[0];
      if (!password_verify($_POST['administradorSenhaAntiga'],$dados['administrador_senha'])) {
        $json["erros"] = "Senha antiga está incorreta!";
        $json["status"] = 0;
        echo json_encode($json);
        exit;
      }

      if ($_POST['administrador_senha']!=$_POST['administradorConfirmaSenha']) {
        $json["erros"] = "Senhas Divergem!";
        $json["status"] = 0;
        echo json_encode($json);
        exit;
      }

       $administrador->setSenha(password_verify($_POST['administrador_senha'],PASSWORD_DEFAULT));
       $administrador->setId($_SESSION['id']);
       if ($administrador->atualizaSenhaAdmin(null)) {
         $json["status"] = 1;
         echo json_encode($json);
       } else {
         $json["erros"] = "Contate o administrador!";
         $json["status"] = 0;
         echo json_encode($json);
         exit;
       }
     }
 


    
  }
