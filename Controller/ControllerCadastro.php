<?php
namespace SISMAREPTB1\Controller;

use SISMAREPTB1\Model\ModelPacientes;
use SISMAREPTB1\View\ViewAdministrador;
use SISMAREPTB1\View\ViewCadastro;

class ControllerCadastro{
    public function __construct($parametro=null){

    }

    public function pacientes(){
        $pacientes = new ModelPacientes();
        $pacientes->setId(null);
        $paciente = $pacientes->listaPacientes();
        $ViewEsqueceuSenha = new ViewCadastro();
        $ViewEsqueceuSenha -> cadastroPacientes($paciente);
    }

    //Função para cadastrar um paciente
   public function ajax_cadastra_paciente()
   {
 
     $json = array();
     $json["status"] = 1;
     $json["erros"] = array();
 
     $pacientes = new ModelPacientes();
     if ($pacientes->Verificar_Duplicidade($_POST['paciente_cpf'], null) > 0) {
       $json["erros"] = "Já existe um paciente cadastrado com esse cpf!";
       $json["status"] = 0;
       echo json_encode($json);
       exit;
     }
     

        $pacientes->setNome($_POST['paciente_nome']);
        $pacientes->setCpf($_POST['paciente_cpf']);
        $pacientes->setTelefone($_POST['paciente_telefone']);
        $pacientes->setDataNascimento($_POST['paciente_data_nascimento']);
        $pacientes->setRua($_POST['paciente_rua']);
        $pacientes->setNumero($_POST['paciente_numero']);
        $pacientes->setBairro($_POST['paciente_bairro']);
        $pacientes->setCidade($_POST['paciente_cidade']);
        $pacientes->setEstado($_POST['paciente_estado']);
        $pacientes->setLocalTratamento($_POST['paciente_local_tratamento']);
        $pacientes->setObservacao($_POST['paciente_observacao']);
        $pacientes->setDataAdmissao($_POST['paciente_data_admissao']);
     if ($pacientes->cadastraPaciente()) {
       $json["status"] = 1;
       echo json_encode($json);
     } else {
       $json["erros"] = "Contate o administrador!";
       $json["status"] = 0;
       echo json_encode($json);
       exit;
     }
   }
    
    


    
}
