<?php

namespace SISMAREPTB1\View;


class ViewAdministrador
{
  public function __construct()
  {
  }
  public function listarAdministradores($administradores)
  {
    $scripts =  array(
        "mascaras.js",
      "administradores.js"
    );
    $styles =  array(
      "estilo.css"
    );
    $titulo = "Administradores";
    $pagina = "4";
    $super_usuario = $_SESSION['super_usuario'];
    $listaadministrador = '';
    foreach ($administradores as $administrador) {

      $listaadministrador = $listaadministrador . "
            <tr>
              <td>" . $administrador['administrador_id'] . "</td>
              <td>" . $administrador['administrador_nome'] . "</td>
              <td>" . $administrador['administrador_telefone'] . "</td>
              <td>
              <button class='btn btn-danger btn-deletar-administrador' administrador='" . $administrador['administrador_id'] . "'>
                <i class='fa fa-trash-alt'></i>
              </button></td>
            </tr>
             ";
    }
    $conteudo1 = '
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Administradores</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="/SISMAREPTB1/Inicio/agenda">Início</a></li>
                            <li class="breadcrumb-item active">Administradores</li>
                        </ol>
                    </div>
                </div>
            </div><br><!-- /.container-fluid -->
            <button type="submit" id="btn_cadastro_administrador" class="btn btn-primary ml-2"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Novo Administrador</button>
        </section>
   
        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- /.row -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Listagem de Administradores</h3>
   
                                <div class="card-tools">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                        <input type="text" name="table_search" alt="tabela_administradores" class="form-control float-right pesquisa" placeholder="Pesquisar">
   
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body table-responsive p-0">
                                <table class="table table-hover text-nowrap tabela_administradores" id="tabela_administradores">
                                    <thead>
                                        <tr>
                                            <th style="width: 10%;">Cod.</th>
                                            <th style="width: 60%;">Nome</th>
                                            <th style="width: 20%;">Telefone</th>
                                            <th style="width: 10%;">Excluir</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                  ' . $listaadministrador . '
                  </tbody>
                  </table>
              </div>
              <!-- /.card-body -->
            </div>
          <!-- /.card -->
        </div>
      </div>

      <!-- /.row -->
    </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

      </div>

      <div id="modal_administradores" class="modal">
          <div class="modal-dialog modal-lg">
              <div class="modal-content">
                  <div class="modal-header">
                      <h5 class="modal-title">Administradores</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                          <span aria-hidden="true">&times;</span>
                  </div>

                  <div class="modal-body">

                      <form action="" method="POST" name="form_cadastro_administradores" id="form_cadastro_administradores" data-toggle="validator">
                          <div class="form-row">
                              <div class="col-md-8">
                                  <label> Nome do Administrador:</label>
                                  <div class="iconInput">
                                      <i class="fa fa-user-shield"></i>
                                      <input type="text" name="administrador_nome" id="administrador_nome" class="form-control " placeholder="Digite o nome do administrador" required>
                                  </div>
                              </div>
                          </div>
                          <br>
                          <div class="form-row">
                              <div class="col-md-8">
                                  <label> Email:</label>
                                  <div class="iconInput">
                                      <i class="fa fa-envelope"></i>
                                      <input type="email" name="administrador_email" id="administrador_email" class="form-control " placeholder="Digite o email do administrador" required>
                                  </div>
                              </div>
                          </div>
                          <br>
                          <div class="form-row">
                              <div class="col-md-8">
                                  <label> Telefone:</label>
                                  <div class="iconInput">
                                      <i class="fa fa-phone"></i>
                                      <input type="text" name="administrador_telefone" id="administrador_telefone" class="form-control " placeholder="Digite telefone do administrador" required minlength="10" maxlength="11">
                                  </div>
                              </div>
                          </div>
                          <br>
                          <div class="form-row">
                              <div class="col-md-8">
                                  <label> Senha Inicial:</label>
                                  <div class="iconInput">
                                      <i class="fa fa-key"></i>
                                      <input type="password" name="administrador_senha" id="administrador_senha" class="form-control " placeholder="Digite a senha inicial do administrador" required minlength="6">
                                  </div>
                              </div>
                          </div>
                          <br>
                          <div class="form-row">
                              <div class="col-md-8">
                                  <label>Confirmar Senha:</label>
                                  <div class="iconInput">
                                      <i class="fa fa-key"></i>
                                      <input type="password" name="confirmaSenha" id="confirmaSenha" class="form-control " placeholder="Digite novamente a senha" required minlength="6">
                                  </div>
                              </div>
                          </div>

                          <br>

                          <div class="form-row">
                              <div class="col-md-8">
                                  <label> Super Usuário?</label>
                                  <div class="iconInput">
                                      <div class="form-check form-check-inline">
                                          <input class="form-check-input" type="radio" name="administrador_super_usuario" id="administrador_super_usuario" value="0" checked>
                                          <label class="form-check-label" for="inlineRadio1">Não</label>
                                      </div>
                                      <div class="form-check form-check-inline">
                                          <input class="form-check-input" type="radio" name="administrador_super_usuario" id="administrador_super_user" value="1">
                                          <label class="form-check-label" for="inlineRadio2">Sim</label>
                                      </div>

                                  </div>
                              </div>
                          </div>

                          <br>

                          <div class="form-row ">
                              <div class="col-md-6">
                                  <button type="submit" id="btn_cadastro_administrador" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
                                  <span class="help-block"></span>
                              </div>
                          </div>


                      </form>
                  </div>


              </div>
          </div>
      </div>
      </div>
        ';

    $administrador = $_SESSION['nome'];
    include 'Templates/template.php';
  }
}
