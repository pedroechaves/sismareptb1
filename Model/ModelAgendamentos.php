<?php
namespace SISMAREPTB1\Model;
use PDO;
use PDOException;

class ModelAgendamentos
{
  private $id;
  private $email;
  private $data;
  private $medico;
  private $paciente;
  private $preco;
  private $hora;
  private $sintomas;

  public function getId()
  {
    return $this->id;
  }

  public function setId($id)
  {
    $this->id = $id;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public function setEmail($email)
  {
    $this->email = $email;
  }

  public function getData()
  {
    return $this->data;
  }

  public function setData($data)
  {
    $this->data = $data;
  }

  public function getMedico()
  {
    return $this->medico;
  }

  public function setMedico($medico)
  {
    $this->medico = $medico;
  }

  public function getPaciente()
  {
    return $this->paciente;
  }

  public function setPaciente($paciente)
  {
    $this->paciente = $paciente;
  }

  public function getPreco()
  {
    return $this->preco;
  }

  public function setPreco($preco)
  {
    $this->preco = $preco;
  }

  public function getHora()
  {
    return $this->hora;
  }

  public function setHora($hora)
  {
    $this->hora = $hora;
  }

  public function getSintomas()
  {
    return $this->sintomas;
  }

  public function setSintomas($sintomas)
  {
    $this->sintomas = $sintomas;
  }

  //Retorna os agendamentos de determinada data
	//
	public function listaAgendamentos()
	{
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
      if (!empty($this->data)) {
        $sql = "select agendamentos.agendamento_id,agendamentos.agendamento_data,agendamentos.agendamento_hora,pacientes.paciente_nome,medicos.medico_nome
        from agendamentos inner join medicos on medicos.medico_id=agendamentos.medico_id join pacientes on pacientes.paciente_id=agendamentos.paciente_id where agendamento_data='$this->data'";
      } else {
        $dataHoje = date("Y-m-d");
        $sql = "select agendamentos.agendamento_id,agendamentos.agendamento_data,agendamentos.agendamento_hora,pacientes.paciente_nome,medicos.medico_nome
        from agendamentos inner join medicos on medicos.medico_id=agendamentos.medico_id join pacientes on pacientes.paciente_id=agendamentos.paciente_id where agendamento_data='$dataHoje'";
      }

      if ($data = $con->query($sql)) {
        return $data->fetchAll(PDO::FETCH_ASSOC);
      } else {
        return false;
      }
    } catch (PDOException $pdoex) {
      return false;
    }
		

  }
  

  //Retorna os medicos cadastrados
	//
	public function listaMedicos()
	{
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
        $sql = "select * from medicos";
      if ($data = $con->query($sql)) {
        return $data->fetchAll(PDO::FETCH_ASSOC);
      } else {
        return false;
      }
    } catch (PDOException $pdoex) {
      return false;
    }
		

  }
  
  //Verifica a disponibilidade de horarios na data selecionada
	//
	public function Verificar_Disponibilidade($agendamentoData,$agendamentoHora)
	{
		$user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass); 
      $sql = $con->query("select * from agendamentos where agendamento_data='$agendamentoData' and agendamento_hora='$agendamentoHora'")->fetchAll();
    
      return count($sql);
    } catch (PDOException $pdoex) {
      return false;
    }
	
  }
  

  //Inserindo um novo agendamento
  public function insereAgendamento()
  {
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
      $sql = 'insert into agendamentos (paciente_id, agendamento_data,agendamento_hora,medico_id,agendamento_preco,agendamento_sintomas,paciente_email) values (?, ?, ?, ?, ?, ?, ?);';
      $pre = $con->prepare($sql);
      $pre->bindValue(1, $this->paciente);
      $pre->bindValue(2, $this->data);
      $pre->bindValue(3, $this->hora);
      $pre->bindValue(4, $this->medico);
      $pre->bindValue(5, $this->preco);
      $pre->bindValue(6, $this->sintomas);
      $pre->bindValue(7, $this->email);
      if ($pre->execute()) {
        return true;
      } else {
        return false;
      }
    } catch (PDOException $pdoex) {
      return false;
    }
  }


  //Retorna os agendamentos do Id
	//
	public function listaAgendamentosId()
	{
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
      if (!empty($this->id)) {
        $sql = "select agendamentos.agendamento_id,agendamentos.agendamento_preco,agendamentos.agendamento_data,agendamentos.agendamento_hora,agendamentos.agendamento_sintomas,pacientes.paciente_nome,pacientes.paciente_telefone,medicos.medico_nome
        from agendamentos inner join medicos on medicos.medico_id=agendamentos.medico_id join pacientes on pacientes.paciente_id=agendamentos.paciente_id where agendamento_id='$this->id'";
      } else {
        
        $sql = "select agendamentos.agendamento_id,agendamentos.agendamento_data,agendamentos.agendamento_hora,pacientes.paciente_nome,medicos.medico_nome
        from agendamentos inner join medicos on medicos.medico_id=agendamentos.medico_id join pacientes on pacientes.paciente_id=agendamentos.paciente_id";
      }

      if ($data = $con->query($sql)) {
        return $data->fetchAll(PDO::FETCH_ASSOC);
      } else {
        return false;
      }
    } catch (PDOException $pdoex) {
      return false;
    }
		

  }

  //
  ///Exclui um agendamento
  //
  public function excluiAgendamento()
  {
    $user = 'root';
    $pass = '';
    try {
      $con = new PDO('mysql:server=localhost;dbname=sismarep;port=3306', $user, $pass);
      $sql = 'DELETE FROM `agendamentos` WHERE agendamento_id = ' . $this->id . ';';
      if ($data = $con->query($sql)) {
        return true;
      } else {
        return false;
      }
    } catch (PDOException $pdoex) {
      return false;
    }
  }


}




?>