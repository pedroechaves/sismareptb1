<?php
  namespace SISMAREPTB1\Controller;
  use SISMAREPTB1\View\ViewListagemPaciente;
  use SISMAREPTB1\Model\ModelPacientes;

  class ControllerListagemPaciente{
    public function __construct($parametro=null){
    }

    public function pacientes(){
      $pacientes = new ModelPacientes();
      $pacientes->setId(null);
      $paciente = $pacientes->listaPacientes();
      $ViewEsqueceuSenha = new ViewListagemPaciente();
      $ViewEsqueceuSenha->listarpacientes($paciente);
    }

    public function ajax_excluir_paciente()
	{

		$json = array();
		$json["status"] = 1;
		$json["listaErros"] = array();
        $pacientes = new ModelPacientes();
        $pacientes->setId($_POST['paciente_numero']);
		if ($pacientes->excluiPacientes()) {
			$json["status"] = 1;
		} else {
			$json["listaErros"] = "Contate o administrador!";
			$json["status"] = 0;
			echo json_encode($json);
			exit;
		}


		echo json_encode($json);
  }
  
  

    
    
  }
