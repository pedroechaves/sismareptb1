<?php

namespace SISMAREPTB1\View;

if (!isset($_SESSION)) {
    session_start();
}

if (!$_SESSION['login'] == true) {
    echo "
    <script>
      window.location.href = 'http://localhost/SISMAREPTB1/'
    </script>
  ";
};
class ViewInicio
{
    public function __construct()
    {
    }

    public function listar($medicos,$paciente)
    {
        $scripts =  array(
            "jquery.maskMoney.js",
            "agendamento.js"
        );
        $styles =  array(
            "detalhesagenda.css",
            "estilo.css"
        );
        $titulo = "Página Inicial";
        $pagina = "1";
        $listamedicos = '';
        $listapaciente = '';
        $super_usuario = $_SESSION['super_usuario'];
        foreach ($medicos as $med) {
            $listamedicos = $listamedicos . '
            <option value='.$med['medico_id'].'>'.$med['medico_nome'].' / '.$med['medico_especialidade'].'</option>
            
            ';
        }
        foreach ($paciente as $pac) {
            $listapaciente = $listapaciente . '
            <option value='.$pac['paciente_id'].'>'.$pac['paciente_nome'].'</option>
            
            ';
        }
        $conteudo1 = '
      
        <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
     <!-- Content Header (Page header) -->
     <section class="content-header">
         <div class="container-fluid">
             <div class="row mb-2">
                 <div class="col-sm-6">
                     <h1>Agenda</h1>
                 </div>
                 <div class="col-sm-6">
                     <ol class="breadcrumb float-sm-right">
                         <li class="breadcrumb-item"><a href="/SISMAREPTB1/Inicio/agenda">Início</a></li>
                         <li class="breadcrumb-item active">Agenda</li>
                     </ol>
                 </div>
             </div>
         </div><br><!-- /.container-fluid -->
         <button type="submit" id="btn_cadastro_agendamento" class="btn btn-primary ml-2"><i class="fa fa-plus-square"></i>&nbsp;&nbsp;Novo Agendamento</button>
     </section>
        <section class="content">
        
          <div class="container-fluid">
              <!-- /.row -->
              <div class="row">
                  <div class="col-12">
                      <div class="card">
                          <div class="card-header">
                              <h3 class="card-title" id="titulo_data">'.date("d/m/Y").'</h3>

                              <div class="card-tools">
                                  <div class="input-group input-group-sm iconInput" style="width: 150px;">
                                      <input type="date" name="table_search" id="datadia" class="form-control float-right pesquisa" value=' . date("Y-m-d") . ' placeholder="Data">
                                  </div>

                              </div>
                          </div>
                          
                          <!-- /.card-header -->
                          <div class="card-body table-responsive p-0">
                              <table class="table table-hover text-nowrap ">
                                  <thead>
                                      <tr>
                                          <th style="width: 10%;">Horário</th>
                                          <th style="width: 40%;">Paciente</th>
                                          <th style="width: 35%;">Médico</th>
                                          <th style="width: 15%;">Ações</th>
                                      </tr>
                                  </thead>
                                  <tbody id="agendamentos">

                                  </tbody>
                              </table>
                          </div>
                          <!-- /.card-body -->
                      </div>
                      <!-- /.card -->
                  </div>
              </div>

              <!-- /.row -->
          </div><!-- /.container-fluid -->
          
      </section>
      <!-- /.content -->
  </div>

  <div id="modalcadastroagenda" class="modal">
      <div class="modal-dialog modal-lg">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title">Cadastro de Agenda</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                      <span aria-hidden="true">&times;</span>
              </div>

              <div class="modal-body">

                  <form action="" method="POST" name="form_cadastro_agenda" id="form_cadastro_agenda" data-toggle="validator">
                      <div class="form-row">
                          <div class="col-md-8">
                              <label> Paciente:</label>
                              <select class="custom-select mr-sm-2" id="paciente_id" name="paciente_id" required>
                                  '.$listapaciente.'
                              </select>
                          </div>
                      </div>
                      <br>
                      <div class="form-row">
                          <div class="col-md-8">
                              <label> Medico:</label>
                              <select class="custom-select mr-sm-2" id="medico_id" name="medico_id" required>
                              '.$listamedicos.'
                              </select>
                          </div>
                      </div>
                      <br>
                      <div class="form-row">
                          <div class="col-md-6">
                              <label> Valor da Consulta:</label>
                              <div class="iconInput">
                                  <i class="fa fa-money-bill-alt"></i>
                                  <input type="text" name="agendamento_preco" id="agendamento_preco" class="form-control " placeholder="Digite o valor da consulta" required>
                              </div>
                          </div>
                      </div>
                      <br>
                      <div class="form-row">
                          <div class="form-group col-md-4 ">
                              <label for="inputCity">Data:</label>
                              <div class="iconInput">
                                  <i class="fa fa-calendar-week"></i>
                                  <input type="date" name="agendamento_data" id="agendamento_data" class="form-control" required min="'.date("Y-m-d").'">
                              </div>
                          </div>
                          <div class="form-group col-md-4">
                              <label> Hora:</label>
                              <select class="custom-select mr-sm-2" id="agendamento_hora" name="agendamento_hora" required>
                                  <option value="" disabled selected>Selecione</option>
                                  <option value="08:00">08:00</option>
                                  <option value="09:00">09:00</option>
                                  <option value="10:00">10:00</option>
                                  <option value="11:00">11:00</option>
                                  <option value="12:00">12:00</option>
                                  <option value="13:00">13:00</option>
                                  <option value="14:00">14:00</option>
                                  <option value="15:00">15:00</option>
                                  <option value="16:00">16:00</option>
                                  <option value="17:00">17:00</option>
                                  <option value="18:00">18:00</option>
                              </select>
                          </div>
                      </div>
                      <div class="form-row">
                          <div class="col-md-6">
                              <label> Sintomas:</label>
                              <div class="iconInput">
                                  <textarea name="agendamento_sintomas" id="agendamento_sintomas" class="col-md-12">
                              </textarea>

                              </div>
                          </div>
                      </div>
                      <br>

                      <div class="form-row">
                          <div class="col-md-8">
                              <label> Email para confirmação:</label>
                              <div class="iconInput">
                                  <i class="fa fa-envelope"></i>
                                  <input type="email" name="paciente_email" id="paciente_email" class="form-control " placeholder="Digite o email para contato" required>
                              </div>
                          </div>
                      </div>
                      <br>
                      <div class="form-row ">
                          <div class="col-md-6">
                              <button type="submit" id="btn_hr" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;&nbsp;Cadastrar</button>
                              <span class="help-block"></span>
                          </div>
                      </div>


                  </form>
              </div>


          </div>
      </div>
  </div>
  </div>



  <div class="modal fade" id="modalinfoconsulta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Detalhes do agendamento</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="modal-body">
                  <main class="page payment-page">
                      <section class="payment-form dark">
                          <div class="container">
                              <div class="block-heading">
                                  <h2>Detalhes</h2>
                              </div>
                              <form>
                                  <div class="products" id="detalhes">

                                  </div>


                          </div>
                          </form>


              </div>
              </section>

              </main>
          </div>

      </div>
  </div>
  </div>';
        $administrador = $_SESSION['nome'];
        include 'Templates/template.php';
    }
}
