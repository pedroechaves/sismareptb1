$(function () {

	$(document).ready(function () {

		$("#administrador_telefone").mask("(99)999999999");
	});
	/*************************Configurações Administradores*******************************/
	$("#btn_cadastro_administrador").click(function () {
		$("#form_cadastro_administradores")[0].reset();
		$("#modal_administradores").modal();
	});

	/************************Pesquisar na tabela de administradores***************/

	$(".pesquisa").keyup(function () {
		//pega o css da tabela 
		var tabela = $(this).attr('alt');
		if ($(this).val() != "") {
			$("." + tabela + " tbody>tr").hide();
			$("." + tabela + " td:contains-ci('" + $(this).val() + "')").parent("tr").show();
		} else {
			$("." + tabela + " tbody>tr").show();
		}
	});

	$.extend($.expr[":"], {
		"contains-ci": function (elem, i, match, array) {
			return (elem.textContent || elem.innerText || $(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
	});

	/*************************Cadastro de Administrador*******************************/
	$("#form_cadastro_administradores").submit(function () {
		$.ajax({
			type: "POST",
			url: BASE_URL + "FuncoesAdministrador/ajax_cadastra_administrador",
			dataType: "json",
			data: $(this).serialize(),
			success: function (response) {
				if (response["status"]) {
					Swal.fire(
						'Sucesso!',
						'Administrador cadastrado com sucesso!',
						'success'
					)
					$("#tabela_administradores").load(BASE_URL + "Administrador/administradores #tabela_administradores");
					$("#form_cadastro_administradores")[0].reset();
					$("#modal_administradores").modal('hide');


				} else {

					Swal.fire({
						icon: 'error',
						title: 'Erro ao realizar cadastro!',
						text: response["erros"]

					})

				}

			}
		})
		return false;
	});

	/*************************Excluindo um administrador*******************************/
	$(document).on('click', '.btn-deletar-administrador', function () {
		administrador = $(this).attr("administrador");
		Swal.fire({
			title: "Atenção!",
			text: "Deseja excluir esse administrador?",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: "#d9534f",
			confirmButtonText: "Sim",
			cancelButtonText: "Não",
			closeOnConfirm: true,
			closeOnCancel: true,
		}).then((result) => {
			if (result.value) {
				$.ajax({
					type: "POST",
					url: BASE_URL + "FuncoesAdministrador/ajax_remove_administrador",
					dataType: "json",
					data: { "administrador": administrador },
					success: function (response) {
						if (response["status"]) {
							Swal.fire(
								'Sucesso!',
								'Administrador excluido com sucesso!',
								'success'
							)
							$("#tabela_administradores").load(BASE_URL + "Administrador/administradores #tabela_administradores");

						}
						else {
							Swal.fire({
								icon: 'error',
								title: 'Erro ao excluir!',
								text: response["erros"]

							})
						}


					}
				})
			}
		})
	});

	/*************************Atualizando dados um Administrador*******************************/
	$("#form_altera_administradores").submit(function () {
		$.ajax({
			type: "POST",
			url: BASE_URL + "FuncoesAdministrador/ajax_edita_administrador",
			dataType: "json",
			data: $(this).serialize(),
			success: function (response) {
				if (response["status"]) {
					Swal.fire(
						'Sucesso!',
						'Dados atualizados com sucesso!',
						'success'
					)
					$("#name").load(BASE_URL + "MeusDados/ver #name");

				} else {

					Swal.fire({
						icon: 'error',
						title: 'Erro ao atualizar dados!',
						text: response["erros"]

					})

				}

			}
		})


		return false;
	});

	/****************************Abrindo modal para alterar senha******************/
	$("#altera_senha").click(function () {
		$("#formulario_altera_senha")[0].reset();
		$("#modal_altera_senha").modal();
	});

	/*************************Atualizando senha de um Administrador*******************************/
	$("#formulario_altera_senha").submit(function () {
		$.ajax({
			type: "POST",
			url: BASE_URL + "FuncoesAdministrador/ajax_edita_senha",
			dataType: "json",
			data: $(this).serialize(),
			success: function (response) {
				if (response["status"]) {
					Swal.fire(
						'Sucesso!',
						'Senha atualizada com sucesso!',
						'success'
					)
					$("#formulario_altera_senha")[0].reset();
					$("#modal_altera_senha").modal('hide');

				} else {

					Swal.fire({
						icon: 'error',
						title: 'Erro ao atualizar senha!',
						text: response["erros"]

					})

				}

			}
		})


		return false;
	});




});