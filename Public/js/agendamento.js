$(function () {
    $("#agendamento_preco").maskMoney({ thousands: '.', decimal: '.', symbolStay: true });
    $(document).ready(function () {
        lista_agenda(null);
    });
    function lista_agenda(dia) {

        $.ajax({
            type: "POST",
            url: BASE_URL + "Inicio/listaHorarios",
            dataType: "json",
            data: { "diaAtual": dia },
            success: function (data) {

                $("#agendamentos").html(data['horarios']);
                var dataFormatada = dia.replace(/(\d*)-(\d*)-(\d*).*/, '$3/$2/$1');
                $("#titulo_data").html(dataFormatada);



            }
        })
    }

    $('#datadia').change(function () {
        diaselecionado = $(this).val();
        lista_agenda(diaselecionado);

    });

    /**********************Abrir modal de cadastro de agenda***********************/
    $("#btn_cadastro_agendamento").click(function () {
        //$("#form_cadastro_administradores")[0].reset();
        $("#modalcadastroagenda").modal();
    });

    /***************************Verificar disponibilidade de Horários*************/
    $('#agendamento_hora').change(function () {
        var opcao = $('#agendamento_hora option:selected').val();
        var data = $('#agendamento_data').val();
        if (data == '') {
            Swal.fire({
                icon: 'error',
                title: 'Erro!',
                text: 'É preciso selecionar uma data para continuar!'

            })
            $(this).val("");
        }
        else {
            $.ajax({
                type: "POST",
                url: BASE_URL + "Inicio/verificaHorario",
                dataType: "json",
                data: { "dataselecionada": data, "horaselecionada": opcao },
                success: function (response) {
                    if (response['status'] == 0) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Erro!',
                            text: 'Hora selecionada não está disponivel para essa data!'

                        })
                        $('#agendamento_hora').val("");
                    }

                }
            })
        }

    });

    /********************************************Cadastrando um horario na agenda******************************/

    $("#form_cadastro_agenda").submit(function () {
        $.ajax({
            type: "POST",
            url: BASE_URL + "inicio/ajax_cadastra_agendamento",
            dataType: "json",
            data: $(this).serialize(),
            beforeSend: function () {
                $("#loading").css('display', 'block');
            },
            success: function (response) {
                if (response["status"]) {
                    $("#loading").css('display', 'none');
                    Swal.fire(
                        'Sucesso!',
                        'Agendamento realizado com sucesso!',
                        'success'
                    ),
                        $("#modalcadastroagenda").modal('hide');
                    $("#form_cadastro_agenda")[0].reset();
                    lista_agenda(response['datainformada']);
                    $("#datadia").val(response['datainformada']);


                } else {
                    $("#loading").css('display', 'none');
                    Swal.fire({
                        icon: 'error',
                        title: 'Erro ao realizar cadastro!',
                        text: response["listaErros"]

                    })
                    
                }

            }
        })

        return false;
    });


    /*********************************************Detalhes do agendamento*********************/

    $(document).on('click', '.btn_detalhes_agendamento', function () {
        agendamentoId = $(this).attr("agendamento_detalhes");
        $.ajax({
            type: "POST",
            url: BASE_URL + "Inicio/listaAgendamento",
            dataType: "json",
            data: { "agendamento_detalhes": agendamentoId },
            success: function (data) {
                $("#detalhes").html(data['informacoes']);
                $("#modalinfoconsulta").modal();

            }
        })

        return false;
    });

    /************************************************Cancelar agendamento ***********************/
    $(document).on('click', '.btn_cancela_agendamento', function () {
        agendamentoNumero = $(this).attr("agendamento_numero");
        Swal.fire({
            title: "Atenção!",
            text: "Deseja cancelar esse angendamento?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#d9534f",
            confirmButtonText: "Sim",
            cancelButtonText: 'Não',
            closeOnConfirm: true,
            closeOnCancel: true,
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: BASE_URL + "inicio/ajax_cancela_agendamento",
                    dataType: "json",
                    data: { "agendamento_numero": agendamentoNumero },
                    beforeSend: function () {
                        $("#loading").css('display', 'block');
                    },
                    success: function (response) {
                        if (response["status"]) {
                            $("#loading").css('display', 'none');
                            Swal.fire(
                                'Sucesso!',
                                'Agendamento cancelado com sucesso!',
                                'success'
                            ),
                            lista_agenda(response['datainformada']);
                        }
                        else {
                            $("#loading").css('display', 'none');
                            Swal.fire({
                                icon: 'error',
                                title: 'Erro ao excluir!',
                                text: response["listaErros"]

                            })
                        }


                    }
                })
            }
        })

        return false;
    });



});