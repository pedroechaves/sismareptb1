-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 13-Nov-2020 às 22:25
-- Versão do servidor: 10.4.14-MariaDB
-- versão do PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `sismarep`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `administradores`
--

CREATE TABLE `administradores` (
  `administrador_id` int(11) NOT NULL,
  `administrador_nome` varchar(100) NOT NULL,
  `administrador_email` varchar(100) NOT NULL,
  `administrador_senha` varchar(255) NOT NULL,
  `administrador_super_usuario` bigint(20) NOT NULL,
  `administrador_telefone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `administradores`
--

INSERT INTO `administradores` (`administrador_id`, `administrador_nome`, `administrador_email`, `administrador_senha`, `administrador_super_usuario`, `administrador_telefone`) VALUES
(1, 'admin', 'jeffersonwinchester13@gmail.com', '$2y$10$GjPTMxftSMXdqOTVQlDQTO16wi9g7NT3K.R8yHU6Hr9N0qcn/lqnG', 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `agendamentos`
--

CREATE TABLE `agendamentos` (
  `agendamento_id` int(11) NOT NULL,
  `paciente_id` int(11) NOT NULL,
  `agendamento_data` date NOT NULL,
  `agendamento_hora` time NOT NULL,
  `medico_id` int(11) NOT NULL,
  `agendamento_preco` decimal(18,2) NOT NULL,
  `agendamento_sintomas` text NOT NULL,
  `paciente_email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estrutura da tabela `medicos`
--

CREATE TABLE `medicos` (
  `medico_id` int(11) NOT NULL,
  `medico_nome` varchar(100) NOT NULL,
  `medico_especialidade` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Extraindo dados da tabela `medicos`
--

INSERT INTO `medicos` (`medico_id`, `medico_nome`, `medico_especialidade`) VALUES
(1, 'Dr. Enzo', 'Clínico Geral'),
(2, 'Dr Giovanni', 'Cirurgião'),
(3, 'Dr. Valdir', 'Obstetra');

-- --------------------------------------------------------

--
-- Estrutura da tabela `pacientes`
--

CREATE TABLE `pacientes` (
  `paciente_id` int(11) NOT NULL,
  `paciente_nome` varchar(100) NOT NULL,
  `paciente_cpf` varchar(14) NOT NULL,
  `paciente_telefone` varchar(14) NOT NULL,
  `paciente_data_nascimento` date NOT NULL,
  `paciente_rua` varchar(100) NOT NULL,
  `paciente_numero` varchar(5) NOT NULL,
  `paciente_bairro` varchar(100) NOT NULL,
  `paciente_cidade` varchar(100) NOT NULL,
  `paciente_estado` char(2) NOT NULL,
  `paciente_local_tratamento` varchar(100) NOT NULL,
  `paciente_observacao` text DEFAULT NULL,
  `paciente_data_admissao` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `pacientes`
--

INSERT INTO `pacientes` (`paciente_id`, `paciente_nome`, `paciente_cpf`, `paciente_telefone`, `paciente_data_nascimento`, `paciente_rua`, `paciente_numero`, `paciente_bairro`, `paciente_cidade`, `paciente_estado`, `paciente_local_tratamento`, `paciente_observacao`, `paciente_data_admissao`) VALUES
(2, 'Jefferson Felipe Leal Cordeiro', '111.111.111-11', '(33)659685588', '2017-06-11', 'Carlos Prates', '430', 'Planalto', 'Capelinha', 'RJ', 'Capelinha', '                             \r\n                         ', '2020-11-13');

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `administradores`
--
ALTER TABLE `administradores`
  ADD PRIMARY KEY (`administrador_id`);

--
-- Índices para tabela `agendamentos`
--
ALTER TABLE `agendamentos`
  ADD PRIMARY KEY (`agendamento_id`);

--
-- Índices para tabela `medicos`
--
ALTER TABLE `medicos`
  ADD PRIMARY KEY (`medico_id`);

--
-- Índices para tabela `pacientes`
--
ALTER TABLE `pacientes`
  ADD PRIMARY KEY (`paciente_id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `administradores`
--
ALTER TABLE `administradores`
  MODIFY `administrador_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `agendamentos`
--
ALTER TABLE `agendamentos`
  MODIFY `agendamento_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de tabela `medicos`
--
ALTER TABLE `medicos`
  MODIFY `medico_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `pacientes`
--
ALTER TABLE `pacientes`
  MODIFY `paciente_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
